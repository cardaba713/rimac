'use strict';
const axios = require('axios');
const API_URL = require('../config/appConfig').API_URL;
const API_ELEMENT = 'planets';

module.exports.list = async event => {
  let response = {};
  try {
    response = await axios.get(API_URL+API_ELEMENT+'/')
  } catch (error) {
    console.error(error);
  }

  return {
    statusCode: 200,
    body: JSON.stringify(
      response.data
      ,
      null,
      2
    ),
  };

};

module.exports.get = async event => {
  console.log("action: get");
  console.log(event);
  let response = {};
  try {
    response = await axios.get(API_URL+API_ELEMENT+'/'+event.pathParameters.id)
    console.log(response.data);
  } catch (error) {
    console.error(error);
  }

  return {
    statusCode: 200,
    body: JSON.stringify(
      response.data
      ,
      null,
      2
    ),
  };

};