# Reto Rimac
Author: Carlos Terrones España


## Puntos de evaluación:

Minimo 2 endpoints, GET para recuperar la información y POST para crear un elemento\
Integración con una base de datos (DynamoDB o MySQL)\
Uso de Serverless Framework\
Uso de Node.js\
Respeto de las buenas prácticas de desarrollo


## Puntos bonus:
Pruebas unitarias\
Documentación\
Desplegar sin errores en AWS con el comando deploy del framework serverless\
Mayor complejidad de Integración


## Arquitectura
![TArquitectura](exclude/arquitectura.png)


## Pasos para instalacion y despliegue

### Requiere
- serverless
- yarn


### Despliegue

~~~
yarn install
serverless deploy -v
~~~


### Requests con CURL

POST /planets
~~~
curl -X POST "https://nyhm1hjf18.execute-api.us-east-1.amazonaws.com/dev/planets" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"Alderaan\", \"rotation_period\": \"24\", \"orbital_period\": \"364\", \"diameter\": \"12500\", \"climate\": \"temperate\", \"gravity\": \"1 standard\", \"terrain\": \"grasslands, mountains\", \"surface_water\": \"40\", \"population\": \"2000000000\", \"created\": \"\", \"edited\": \"\"}"
~~~

GET /planets
~~~
curl -X GET "https://nyhm1hjf18.execute-api.us-east-1.amazonaws.com/dev/planets" -H "accept: application/json"
~~~

GET /planets/{id}
~~~
curl -X GET "https://nyhm1hjf18.execute-api.us-east-1.amazonaws.com/dev/planets/1" -H "accept: application/json"
~~~

GET /planets/rimac
~~~
curl -X GET "https://nyhm1hjf18.execute-api.us-east-1.amazonaws.com/dev/planets/rimac" -H "accept: application/json"
~~~

## Postman Collection
~~~
rimac/exclude/RETO_RIMAC.postman_collection.json
~~~


## Pruebas unitarias con JEST

~~~
yarn run jest
~~~


## NOTAS
- Lenguaje: nodejs12
- Pruebas unitarias usando JEST
- Las dependencias node_modules se despliegan en layer, para reducir el peso de cada lambda desplegado
- Se redirigio node_modules a layer/nodejs/nodemodules, configuracion en .yarnrc

## EVIDENCIAS

### Despliegue
<img src="exclude/deploy.png" title="Deploy" width="650" />

### Test Unitarios
<img src="exclude/jest.png" title="Testing" width="650" />

### Postman

Lista de planetas del mundo StarWar\
<img src="exclude/postman_list_StarWar.png" title="Postman" width="650" />

Obtener un planeta del mundo starwar\
<img src="exclude/postman_list_StarWar.png" title="Postman" width="650" />

Crear un planeta en el mundo Rimac\
<img src="exclude/postman_create_Rimac.png" title="Postman" width="650" />

Lista de planetas del mundo Rimac\
<img src="exclude/postman_list_Rimac.png" title="Postman" width="650" />

### Documentacion:
archivo: swagger.yaml\
<img src="exclude/swagger.png" title="SWAGGER" width="650" />

### Base de Datos
<img src="exclude/dynamodb.png" title="dynamodb" width="650" />
