const PlanetsService = require('../../../src/services/PlanetsService');

describe('Listar Planetas', () => {
  it('Debe retornar statusCode 200', () => {
    return PlanetsService.list({})
    .then(data => {
      expect(data).toBeDefined()
      expect(data.statusCode).toEqual(200)
    })
  })

  it('Deber tener atributos con parametro minimo name', () => {
    return PlanetsService.list({})
    .then(data => {
      expect(data).toBeDefined()
      let test_body = JSON.parse(data.body);
      expect(test_body.results[0].hasOwnProperty('name')).toEqual(true)
    })
  })
})